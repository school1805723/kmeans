# kmeans - simple k-means implementation 

Simple implementation of the [k-means algortihm](https://en.wikipedia.org/wiki/K-means_clustering).
A visualization for each iteration using matplotlib is created by default.

## Requirements

* recent enough python 3
* (matplotlib and numpy (installable via pipenv))

## Usage

```
usage: kmeans.py [-h] [-n CLUSTERS] [-r RANDOM_POINTS] [-d POINT_DIMENSIONS]
                 [--dont-visualize] [--max MAX] [--min MIN]

options:
  -h, --help            show this help message and exit
  -n CLUSTERS, --clusters CLUSTERS
  -r RANDOM_POINTS, --random-points RANDOM_POINTS
  -d POINT_DIMENSIONS, --point-dimensions POINT_DIMENSIONS
  --dont-visualize
  --max MAX
  --min MIN
```

## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
